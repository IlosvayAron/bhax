<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Liskov!</title>
        <keywordset>
            <keyword/>
        </keywordset>
        <!--<cover>
            <para>
                Programozás tankönyvek rövid olvasónaplói.
            </para>
        </cover>-->
    </info>
    
    <section>
        <title>Liskov helyettesítés sértése</title>
        <para>
            Írjunk olyan OO, leforduló Java és C++ kódcsipetet, amely megsérti a Liskov elvet! Mutassunk rá a megoldásra: jobb OO tervezés. (számos példa szerepel az elv megsértésére az UDPROG repóban, lásd pl. source/binom/Batfai-Barki/madarak/)
        </para>
        <para>
          Segédlink: <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_1.pdf">https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_1.pdf (93-99 fólia)</link>
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>
          Elsőként tekintsük át mit is mond ki a Liskov elv. Ha az S a T altípusa akkor minden olyan helyen ahol a T-t kellene használni a legnagyobb egyszerűséggel behelyetesíthatjük az S-t is anélkül, hogy megváltozna a programrész. Az alábbiakban olyan kódokat szemlélhetünk meg, amely sérti ezt az elvet.
        </para>
        <para>
           C++ változat:
           <programlisting language="c++"><![CDATA[
#include <iostream>

using namespace std;

// ez a T az LSP-ben
class Madar {
public:
     virtual void repul() {
          cout << "Mivel madár, ezért tud repülni!" << endl;
     };
};

// ez a két osztály alkotja a "P programot" az LPS-ben
class Program {
public:
     void fgv ( Madar &madar ) {
          madar.repul();
     }
};

// itt jönnek az LSP-s S osztályok
class Sas : public Madar
{ };

class Pingvin : public Madar // ezt úgy is lehet/kell olvasni, hogy a pingvin tud repülni
{ };

int main ( int argc, char **argv )
{
     Program program;
     /*Madar madar;
     program.fgv ( madar );*/

     Sas sas;
     cout << "Sas - ";
     program.fgv ( sas );

     Pingvin pingvin;
     cout << "Pingvin - ";
     program.fgv ( pingvin ); // sérül az LSP, mert a P::fgv röptetné a Pingvint, ami ugye lehetetlen.

}
]]></programlisting>
        </para>
        <mediaobject>
          <imageobject>
              <imagedata fileref="liskov.png" scale="50" />
          </imageobject>
        </mediaobject>
        <para>
          Amint látjuk a program végkimeneteléből is a Liskov elv sérül, hiszen a megfelelő válasz az lenne, hogy a Pingvin nem repül annak ellenére, hogy madár. A helyes megoldás a körültekintőbb/megfontoltabb programírás, azaz a jobb OO tervezés.
        </para>    
    </section>

    <section>
      <title>Szülő-gyerek</title>
        <para>
            Írjunk Szülő-gyerek Java és C++ osztálydefiníciót, amelyben demonstrálni tudjuk, hogy az ősön keresztül csak az ős üzenetei küldhetőek!
        </para>
        <para>
          Segédlink: <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_1.pdf">https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_1.pdf (98. fólia)</link>
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>
          A feladat kérése szerint írtam egy olyan programot, mely bemutatja, hogy a származtatott osztály nem tudja az ősön keresztül a saját üzeneteit küldeni. Azaz "ősön keresztül csak az ős üzenetei küldhetőek"! Demonstrálásként itt a kód is.
        </para>
        <para>
          <command>C++</command>-ban:
          <programlisting><![CDATA[
#include <iostream>

using namespace std;

class Parent {
public:
    int ertek = 2;

};

class Child: public  Parent {
public:
    int ertek1 = 0;
}; 

int main(int argc, char** argv) {
     Parent* szulo = new Child;
     cout << "Ertek= " << szulo->ertek << "\n";
     cout << "Ertek1= " << szulo->ertek1 << "\n";    
    
    return 0;
}]]></programlisting>
      <mediaobject>
          <imageobject>
              <imagedata fileref="szulo.png" scale="50" />
          </imageobject>
      </mediaobject>
        </para>
        <para>
          Ahogyan a képen is látjuk, a hibaüzenet jelzi a fordítás során, hogy nem lehet elérni a <function>Child</function> származtatott osztálynak a tagjait miközben a szülő osztálya a <function>Parent</function>.
        </para>
    </section>  

    <section>
        <title>Anti OO</title>
        <para>
            A BBP algoritmussal<superscript>4</superscript> a Pi hexadecimális kifejtésének a 0. pozíciótól számított 10<superscript>6</superscript>, 10<superscript>7</superscript>, 10<superscript>8</superscript> darab jegyét határozzuk meg C, C++, Java és C# nyelveken és vessük össze a futási időket!
        </para>
        <para>
           Segédlink: <link xlink:href="https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/apas03.html#id561066">https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/apas03.html#id561066</link>      
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>
            Az eredményül kapott számjegyeket és a futási idejűk értékét az alábbi táblázat tartalmazza, mely alapján könnyen látható a különböző nyelveken megírt program számolási ideje. Lentebb láthatóak a kódok és a futattás folyamata is.
        </para>
        <para>
          <table frame="all">
                <title/>
                <tgroup cols="5">
                    <colspec colname="c1" colnum="1" colwidth="1*"/>
                    <colspec colname="c2" colnum="2" colwidth="1*"/>
                    <colspec colname="c3" colnum="3" colwidth="1*"/>
                    <colspec colname="c4" colnum="4" colwidth="1*"/>
                    <colspec colname="c5" colnum="5" colwidth="1*"/>
                    <thead>
                        <row>
                            <entry/>
                            <entry>Java</entry>
                            <entry>C</entry>
                            <entry>C#</entry>
                            <entry>C++</entry>
                        </row>
                    </thead>
                    <tbody>
                        <row>
                            <entry>10^6</entry>
                            <entry>2.267</entry>
                            <entry>2.511684</entry>
                            <entry>2,417182</entry>
                            <entry>2.87865</entry>
                        </row>
                        <row>
                            <entry>10^7</entry>
                            <entry>26.376</entry>
                            <entry>29.489632</entry>
                            <entry>28,119818</entry>
                            <entry>33.2025</entry>
                        </row>
                        <row>
                            <entry>10^8</entry>
                            <entry>305.729</entry>
                            <entry>340.252091</entry>
                            <entry>322,403944</entry>
                            <entry>379.613</entry>
                        </row>
                    </tbody>
                </tgroup>
            </table>
      <!--    <figure>
          <title><command>Bpp algoritmus futási ideje</command></title>  
          <mediaobject>
              <imageobject>
                  <imagedata fileref="tablazat.png" scale="50" />
              </imageobject>
          </mediaobject>
        </figure>-->
        </para>
        <para>
            A kód Java-ban:
           <programlisting language="java"><![CDATA[ 
        /*
 * PiBBPBench.java
 *
 * DIGIT 2005, Javat tanítok
 * Bátfai Norbert, nbatfai@inf.unideb.hu
 *
 */
/**
 * A PiBBP.java-ból kivettük az "objektumorientáltságot", így kaptuk
 * ezt az osztályt.
 *
 * (A PiBBP osztály a BBP (Bailey-Borwein-Plouffe) algoritmust a Pi hexa
 * jegyeinek számolását végző osztály. A könnyebb olvahatóság
 * kedvéért a változó és metódus neveket megpróbáltuk az algoritmust
 * bemutató [BBP ALGORITMUS] David H. Bailey: The BBP Algorithm for Pi.
 * cikk jelöléseihez.)
 *
 * @author Bátfai Norbert, nbatfai@inf.unideb.hu
 * @version 0.0.1
 */
public class PiBBPBench {
    /**
     * BBP algoritmus a Pi-hez, a [BBP ALGORITMUS] David H. Bailey: The
     * BBP Algorithm for Pi. alapján a {16^d Sj} részlet kiszámítása.
     *
     * @param   d   a d+1. hexa jegytől számoljuk a hexa jegyeket
     * @param   j   Sj indexe
     */
    public static double d16Sj(int d, int j) {
        
        double d16Sj = 0.0d;
        
        for(int k=0; k<=d; ++k)
            d16Sj += (double)n16modk(d-k, 8*k + j) / (double)(8*k + j);
        
        /* (bekapcsolva a sorozat elejen az első utáni jegyekben növeli pl.
            a pontosságot.)
        for(int k=d+1; k<=2*d; ++k)
            d16Sj += Math.pow(16.0d, d-k) / (double)(8*k + j);
         */
        
        return d16Sj - Math.floor(d16Sj);
    }
    /**
     * Bináris hatványozás mod k, a 16^n mod k kiszámítása.
     *
     * @param   n   kitevő
     * @param   k   modulus
     */
    public static long n16modk(int n, int k) {
        
        int t = 1;
        while(t <= n)
            t *= 2;
        
        long r = 1;
        
        while(true) {
            
            if(n >= t) {
                r = (16*r) % k;
                n = n - t;
            }
            
            t = t/2;
            
            if(t < 1)
                break;
            
            r = (r*r) % k;
            
        }
        
        return r;
    }
    /**
     * A [BBP ALGORITMUS] David H. Bailey: The
     * BBP Algorithm for Pi. alapján a
     * {16^d Pi} = {4*{16^d S1} - 2*{16^d S4} - {16^d S5} - {16^d S6}}
     * kiszámítása, a {} a törtrészt jelöli. A Pi hexa kifejtésében a
     * d+1. hexa jegytől
     */
    public static void main(String args[]) {
        
        double d16Pi = 0.0d;
        
        double d16S1t = 0.0d;
        double d16S4t = 0.0d;
        double d16S5t = 0.0d;
        double d16S6t = 0.0d;
        
        int jegy = 0;
        
        long delta = System.currentTimeMillis();
        
        for(int d=1000000; d<1000001; ++d) {
            
            d16Pi = 0.0d;
            
            d16S1t = d16Sj(d, 1);
            d16S4t = d16Sj(d, 4);
            d16S5t = d16Sj(d, 5);
            d16S6t = d16Sj(d, 6);
            
            d16Pi = 4.0d*d16S1t - 2.0d*d16S4t - d16S5t - d16S6t;
            
            d16Pi = d16Pi - Math.floor(d16Pi);
            
            jegy = (int)Math.floor(16.0d*d16Pi);
            
        }
        
        System.out.println(jegy);
        delta = System.currentTimeMillis() - delta;
        System.out.println(delta/1000.0);
    }
}    
                  ]]>
            </programlisting>
        </para>
       <para>
        <figure>
          <title><command>Pi számolás Java-ban</command></title>  
          <mediaobject>
              <imageobject>
                  <imagedata fileref="bpp_java.png" scale="50" />
              </imageobject>
          </mediaobject>
        </figure>  
        </para>
        <para>
          
        </para>
        <para>
            A kód C-ben:
            <programlisting language="c"><![CDATA[ 
#include <stdio.h>
#include <math.h>
#include <time.h>
/*
 * pi_bbp_bench.c
 *
 * DIGIT 2005, Javat tanítok
 * Bátfai Norbert, nbatfai@inf.unideb.hu
 *
 * A PiBBP.java-ból kivettük az "objektumorientáltságot", így kaptuk
 * a PiBBPBench osztályt, amit pedig átírtuk C nyelvre.
 *
 */

/*
 * 16^n mod k
 * [BBP ALGORITMUS] David H. Bailey: The
 * BBP Algorithm for Pi. alapján.
 */
long
n16modk (int n, int k)
{
  long r = 1;

  int t = 1;
  while (t <= n)
    t *= 2;

  for (;;)
    {

      if (n >= t)
    {
      r = (16 * r) % k;
      n = n - t;
    }

      t = t / 2;

      if (t < 1)
    break;

      r = (r * r) % k;

    }

  return r;
}

/* {16^d Sj}
 * [BBP ALGORITMUS] David H. Bailey: The
 * BBP Algorithm for Pi. alapján.
 */
double
d16Sj (int d, int j)
{

  double d16Sj = 0.0;
  int k;

  for (k = 0; k <= d; ++k)
    d16Sj += (double) n16modk (d - k, 8 * k + j) / (double) (8 * k + j);

  /*
     for(k=d+1; k<=2*d; ++k)
     d16Sj += pow(16.0, d-k) / (double)(8*k + j);
   */

  return d16Sj - floor (d16Sj);
}

/*
 * {16^d Pi} = {4*{16^d S1} - 2*{16^d S4} - {16^d S5} - {16^d S6}}
 * [BBP ALGORITMUS] David H. Bailey: The
 * BBP Algorithm for Pi. alapján.
 */
main ()
{

  double d16Pi = 0.0;

  double d16S1t = 0.0;
  double d16S4t = 0.0;
  double d16S5t = 0.0;
  double d16S6t = 0.0;

  int jegy;
  int d;

  clock_t delta = clock ();

  for (d = 1000000; d < 1000001; ++d)
    {

      d16Pi = 0.0;

      d16S1t = d16Sj (d, 1);
      d16S4t = d16Sj (d, 4);
      d16S5t = d16Sj (d, 5);
      d16S6t = d16Sj (d, 6);

      d16Pi = 4.0 * d16S1t - 2.0 * d16S4t - d16S5t - d16S6t;

      d16Pi = d16Pi - floor (d16Pi);

      jegy = (int) floor (16.0 * d16Pi);

    }

  printf ("%d\n", jegy);
  delta = clock () - delta;
  printf ("%f\n", (double) delta / CLOCKS_PER_SEC);
}    
                  ]]>
            </programlisting>
        </para>
        <para>
          <figure>
            <title><command>Pi számolás C-ben</command></title>
            <mediaobject>
              <imageobject>
                  <imagedata fileref="bpp_c.png" scale="50" />
              </imageobject>
            </mediaobject>
          </figure>  
        </para>
        <para>
            A kód C#-ban:
            <programlisting ><![CDATA[ 
                /*
 * FileName: PiBBPBench.cs
 * Author: Bátfai Norbert, nbatfai@inf.unideb.hu
 * DIGIT 2005, Javat tanítok
 */
/// <summary>
/// A PiBBPBench C# átírata.
/// </summary>
/// <remark>
/// A PiBBP.java-ból kivettük az "objektumorientáltságot", így kaptuk
/// a PiBBPBench osztályt, amit pedig átírtuk C# nyelvre.
///
/// (A PiBBP osztály a BBP (Bailey-Borwein-Plouffe) algoritmust a Pi hexa
/// jegyeinek számolását végző osztály. A könnyebb olvahatóság
/// kedvéért a változó és metódus neveket megpróbáltuk az algoritmust
/// bemutató [BBP ALGORITMUS] David H. Bailey: The BBP Algorithm for Pi.
/// cikk jelöléseihez.)
/// </remark>
public class PiBBPBench {
    /// <remark>
    /// BBP algoritmus a Pi-hez, a [BBP ALGORITMUS] David H. Bailey: The
    /// BBP Algorithm for Pi. alapján a {16^d Sj} részlet kiszámítása.
    /// </remark>
    /// <param>
    /// d   a d+1. hexa jegytől számoljuk a hexa jegyeket
    /// </param>
    /// <param>
    /// j   Sj indexe
    /// </param>
    public static double d16Sj(int d, int j) {
        
        double d16Sj = 0.0d;
        
        for(int k=0; k<=d; ++k)
            d16Sj += (double)n16modk(d-k, 8*k + j) / (double)(8*k + j);
        
        /*
        for(int k=d+1; k<=2*d; ++k)
            d16Sj += System.Math.pow(16.0d, d-k) / (double)(8*k + j);
         */
        
        return d16Sj - System.Math.Floor(d16Sj);
    }
    /// <summary>
    /// Bináris hatványozás mod k, a 16^n mod k kiszámítása.
    /// </summary>
    /// <param>
    /// n   kitevő
    /// </param>
    /// <param>
    /// k   modulus
    /// </param>
    public static long n16modk(int n, int k) {
        
        int t = 1;
        while(t <= n)
            t *= 2;
        
        long r = 1;
        
        while(true) {
            
            if(n >= t) {
                r = (16*r) % k;
                n = n - t;
            }
            
            t = t/2;
            
            if(t < 1)
                break;
            
            r = (r*r) % k;
            
        }
        
        return r;
    }
    /// <remark>
    /// A [BBP ALGORITMUS] David H. Bailey: The
    /// BBP Algorithm for Pi. alapján a
    /// {16^d Pi} = {4*{16^d S1} - 2*{16^d S4} - {16^d S5} - {16^d S6}}
    /// kiszámítása, a {} a törtrészt jelöli. A Pi hexa kifejtésében a
    /// d+1. hexa jegytől
    /// </remark>
     public static void Main(System.String[]args) { 
        
        double d16Pi = 0.0d;
        
        double d16S1t = 0.0d;
        double d16S4t = 0.0d;
        double d16S5t = 0.0d;
        double d16S6t = 0.0d;
        
        int jegy = 0;
        
        System.DateTime kezd = System.DateTime.Now;
        
        for(int d=1000000; d<1000001; ++d) {
            
            d16Pi = 0.0d;
            
            d16S1t = d16Sj(d, 1);
            d16S4t = d16Sj(d, 4);
            d16S5t = d16Sj(d, 5);
            d16S6t = d16Sj(d, 6);
            
            d16Pi = 4.0d*d16S1t - 2.0d*d16S4t - d16S5t - d16S6t;
            
            d16Pi = d16Pi - System.Math.Floor(d16Pi);
            
            jegy = (int)System.Math.Floor(16.0d*d16Pi);
            
        }
        
        System.Console.WriteLine(jegy);
        System.TimeSpan delta = System.DateTime.Now.Subtract(kezd);
        System.Console.WriteLine(delta.TotalMilliseconds/1000.0);
    }
}    
                  ]]>
            </programlisting>
        </para>
        <para>
          <figure>
            <title><command>Pi számolás C#-ban</command></title>
            <mediaobject>
              <imageobject>
                  <imagedata fileref="bpp_c_sharp.png" scale="50" />
              </imageobject>
            </mediaobject>
          </figure>  
        </para>
        <para>
            A kód C++-ban:
            <programlisting language="c++"><![CDATA[ 
  #include<iostream>
  #include<math.h>
  #include<time.h>

  long n16MODk(int n, int k) {
    int t = 1;
    
    while(t <= n) {
      t = t * 2;
    }

    if(t > n)
      t = t/2;
    
    long r = 1;

    while(true){
      if(n >= t){
        r = (16*r) % k;
        n = n - t;
      }
      t = t/2;
      if(t < 1)
        break;
      r = (r*r) % k;      
    } 
    return r;
  }

  double d16Sj(int d, int j) {
    double d16Sj = 0.0d;
    for(int i = 0; i <= d; ++i){
      d16Sj += (double)(n16MODk(d - i, 8 * i + j))/(double)(8 * i + j);
    }
    for(int i = d+1; i <= 2*d; ++i) {
      d16Sj += pow(16.0d, d - i) / (double)(8*i + j);
    }
    return d16Sj - floor(d16Sj);
  }
  int main() {
    double d16Pi = 0.0;
    double d16S1t = 0.0;
    double d16S4t = 0.0;
    double d16S5t = 0.0;
    double d16S6t = 0.0;

    int jegy = 0;
    clock_t delta = clock ();
    for(int d=100000000; d<100000001; ++d) {
    d16Pi = 0.0;
    d16S1t = d16Sj(d, 1);
    d16S4t = d16Sj(d, 4);
    d16S5t = d16Sj(d, 5);
    d16S6t = d16Sj(d, 6);

    d16Pi = 4.0*d16S1t - 2.0*d16S4t - d16S5t - d16S6t;
    d16Pi = d16Pi - floor(d16Pi);
    jegy = (int)floor(16.0*d16Pi);
    }

    std::cout << jegy << std::endl;
    delta = clock () - delta;
    std::cout << static_cast<double>(delta)/CLOCKS_PER_SEC << std::endl;
  }  
                  ]]>
            </programlisting>
        </para>
        <para>
          <figure>
            <title><command>Pi számolás C++-ban</command></title>
            <mediaobject>
              <imageobject>
                  <imagedata fileref="bpp_cpp.png" scale="50" />
              </imageobject>
            </mediaobject>
          </figure>  
        </para>  
    </section>

  <!--  <section>
      <title>Hello, Android!</title>
        <para>
            Élesszük fel az SMNIST for Humans projektet! <link xlink:href="https://gitlab.com/nbatfai/smnist/tree/master/forHumans/SMNISTforHumansExp3/app/src/main">https://gitlab.com/nbatfai/smnist/tree/master/forHumans/SMNISTforHumansExp3/app/src/main</link> Apró módosításokat eszközölj benne, pl. színvilág.
        </para>
        <para>    
            Megoldás forrása: <link xlink:href=""></link>
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>
          
        </para>
    </section> -->

    <section>
      <title>Ciklomatikus komplexitás</title>
        <para>
            Számoljuk ki valamelyik programunk függvényeinek ciklomatikus komplexitását! Lásd a fogalom tekintetében a <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_2.pdf">https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_2.pdf (77-79 fóliát)!</link>
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>
          A ciklomatikus komplexitás - továbbiakban CK - kiszámítása annyit tesz mint egy vezérlési gráfban meghatározni a független utak maximális számát. Tehát a számítás alapját a gráf képzi. De akkor mégis, hogyan lehet kiszámítani egy program ciklomatikus komplexitását? A válasz egyszerű! A gráfot a program függvényeiben lévő utasítások alkotját míg az éleit az utasítások között helyezkedik el. Azaz, két utasítás között akkor van él ha az egyik után a másik rögtön végrehajtatódhat. A CK kiszámítását az LZWBinFa C++ és a Polárgenerátor Java változatán végeztem el a <link xlink:href="http://www.lizard.ws/?fbclid=IwAR0DPFH8KjKJ79kBqk6_-ofpvRWyQ6dtAUoQx_b1aqO_pRRnaAK-wHAgP-M">Lizard code complexity analyzer</link> online változatával.
        </para>
        <para>
          <figure>
            <title><command>PolárGenerátor.java</command></title>
            <mediaobject>
              <imageobject>
                  <imagedata fileref="liz_polgen.png" scale="50" />
              </imageobject>
            </mediaobject>
        </figure>
        </para>
        <para>
          <figure>
            <title><command>LZWBinFa.cpp</command></title>
            <mediaobject>
              <imageobject>
                  <imagedata fileref="liz_binfa1.png" scale="50" />
              </imageobject>
            </mediaobject>
          </figure>  
        </para>
        <para>
          <figure>
            <title><command>LZWBinFa.cpp</command></title>  
            <mediaobject>
              <imageobject>
                  <imagedata fileref="liz_binfa2.png" scale="50" />
              </imageobject>
            </mediaobject>
          </figure>  
        </para>
    </section>             
</chapter>                
